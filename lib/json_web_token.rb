# frozen_string_literal: true

require 'jwt'

###
# Wrapper class for JWT functionality
##
class JsonWebToken
  # Encodes and signs JWT payload with expiration
  def self.encode(payload)
    payload.reverse_merge!(meta)
    JWT.encode(payload, Rails.application.credentials.secret_key_base)
  end

  # Decodes the JWT with signed secret
  def self.decode(token)
    JWT.decode(token, Rails.application.credentials.secret_key_base)
  end

  def self.valid_payload(payload)
    expired = expired(payload)
    iss_matches = payload['iss'] == meta[:iss]
    aud_matches = payload['aud'] == meta[:aud]

    return false if expired || !iss_matches || !aud_matches

    true
  end

  # Default options to be encoded in the token
  def self.meta
    {
      exp: 7.days.from_now.to_i,
      iss: 'issuer_name',
      aud: 'client'
    }
  end

  # Validates if the token is expired by exp parameter
  def self.expired(payload)
    Time.at(payload['exp']) < Time.now
  end
end
