import React, { SyntheticEvent } from 'react'
import * as ReactDOM from 'react-dom'
import {
  IAvailableIngredient,
  IIngredient,
  IRecipe
} from './types'

interface IProps {
  availableIngredients: IAvailableIngredient[]
}

interface IState {
  availableIngredients: IAvailableIngredient[]
  rows: number
}

class CreateRecipeForm extends React.Component<IProps, IState> {

  public state: IState

  public crsfToken: string

  constructor (props: IProps) {
    super(props)

    this.state = {
      availableIngredients: props.availableIngredients,
      rows: 1
    }

    this.crsfToken = document.querySelector('meta[name=csrf-token]')!
      .getAttribute('content') || ''
  }

  public handleClick = (e: SyntheticEvent) => {
    e.preventDefault()
    const { rows } = this.state
    this.setState({ rows: rows + 1 })
  }

  public handleClickBack = (e: SyntheticEvent) => {
    e.preventDefault()
    window.history.back()
  }

  public handleRemoveRow = (e: SyntheticEvent) => {
    e.preventDefault()
    const { rows } = this.state
    this.setState({ rows: rows - 1 })
  }

  public renderAvailableIngredientOption (ing: IAvailableIngredient, i: number) {
    return (
      <option
        key={`${ing}-${i}`}
        value={ing.id}
      >
        {ing.name}
      </option>

    )
  }

  public renderIngredient (index: number) {
    const { availableIngredients } = this.props

    return (
      <div
        className='flex flex-row items-center'
        key={`${new Date().getMilliseconds()}-${index}`}
      >
        <select
          className='form-field-inline bg-white flex-1'
          name={`recipe[ingredient_recipes_attributes][${index}][ingredient_id]`}
        >
          {availableIngredients.map((ing, i) => this.renderAvailableIngredientOption(ing, i))}
        </select>
        <input
          className='form-field-inline flex-1'
          name={`recipe[ingredient_recipes_attributes][${index}][quantity]`}
          type='number'
        />
        <button
          className='btn btn-blue-rev h-full flex-1'
          onClick={this.handleRemoveRow}
        >
          -
        </button>
      </div>
    )
  }

  public renderNewIngredients () {
    const { rows } = this.state
    const ingredients = []
    for (let i = 0; i < rows; i++) {
      ingredients.push(this.renderIngredient(i))
    }
    return ingredients
  }

  public render () {
    return (
      <div className='form-container'>
        <h1 className='py-2 mb-2'>New Recipe</h1>
        <form action='/recipes' method='post'>
          <div>
            <label htmlFor='name'>Name</label>
            <input
              className='form-field'
              name='recipe[name]'
              type='text'
              required={true}
            />
          </div>
          <div>
            <label htmlFor='description'>Description</label>
            <input
              className='form-field'
              name='recipe[description]'
              type='textarea'
            />
          </div>
          <div className='mb-2'>
            <span>Ingredients</span>
          </div>
          {this.renderNewIngredients()}
          <button
            className='btn btn-blue w-full'
            onClick={this.handleClick}
          >
            Add Ingredient
          </button>
          <div className='flex mt-4'>
            <button
              className='btn btn-blue btn-group-left flex-1'
              type='submit'
            >
              Create
            </button>
            <button
              className='btn btn-blue-rev btn-group-right flex-1'
              onClick={this.handleClickBack}
            >
              Back
            </button>
          </div>
          <input
            type='hidden'
            name='authenticity_token'
            value={this.crsfToken}
          />
        </form>
      </div>
    )
  }
}

document.addEventListener('DOMContentLoaded', () => {
  const node = document.getElementById('create-recipe-form')!
  const data = JSON.parse(node.getAttribute('data') || '')

  ReactDOM.render(<CreateRecipeForm {...data} />, node)
})
