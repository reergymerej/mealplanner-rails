import React, { createRef, SyntheticEvent } from 'react'
import * as ReactDOM from 'react-dom'
import {
  IAvailableIngredient,
  IIngredient,
  IRecipe
} from './types'

interface IProps {
  availableIngredients: IAvailableIngredient[]
  ingredients: IIngredient[]
  recipe: IRecipe
}

interface IState {
  availableIngredients: IAvailableIngredient[]
  recipe: IRecipe
  newIngredients: number
  ingredients: IIngredient[]
}

class EditRecipeForm extends React.Component<IProps, IState> {
  public static defaultProps = {
    ingredients: []
  }

  private nameField = createRef<HTMLInputElement>()
  private descriptionField = createRef<HTMLInputElement>()

  private crsfToken: string

  constructor (props: IProps) {
    super(props)

    this.state = {
      availableIngredients: props.availableIngredients,
      ingredients: props.ingredients,
      newIngredients: 0,
      recipe: { ...props.recipe }
    }

    this.crsfToken = document.querySelector('meta[name=csrf-token]')!
      .getAttribute('content') || ''
  }

  public componentDidMount () {
    this.nameField.current!.value = this.props.recipe.name
    this.descriptionField.current!.value = this.props.recipe.description || ''
  }

  public getStartingNewIndex () {
    return this.state.ingredients.length
  }

  public handleClickAddIngredient = (e: SyntheticEvent) => {
    e.preventDefault()
    this.setState((prevState) => (
      { newIngredients: prevState.newIngredients + 1 }
    ))
  }

  public handleClickBack = (e: SyntheticEvent) => {
    e.preventDefault()
    window.history.back()
  }

  public handleRemoveIngredient = (index: number) => (e: SyntheticEvent) => {
    e.preventDefault()
    this.setState((prevState) => (
      {
        ingredients: [
          ...prevState.ingredients.slice(0, index),
          ...prevState.ingredients.slice(index + 1, 0)
        ]
      }
    ))
  }

  public handleRemoveNewIngredient = (e: SyntheticEvent) => {
    e.preventDefault()
    this.setState((prevState) => (
      { newIngredients: prevState.newIngredients - 1 }
    ))
  }

  public renderIngredients () {
    const { ingredients } = this.props
    return ingredients.map((el, i) => (
      <div
        className='flex flex-row items-center justify-between my-4'
        key={`${el.id}-${i}`}
      >
        <input
          type='hidden'
          name={`recipe[ingredient_recipes_attributes][${i}][ingredient_id]`}
          value={`${el.ingredient_id}`}
        />
        <input
          type='hidden'
          name={`recipe[ingredient_recipes_attributes][${i}][quantity]`}
          value={`${el.quantity}`}
        />
        <div>
          <span className='font-bold mb-2 mr-2'>{el.name}</span>
          <span>{`${el.quantity} ${el.unit}(s)`}</span>
        </div>
        <button
          className='btn btn-blue-rev h-full'
          onClick={this.handleRemoveIngredient(i)}
        >
          -
        </button>
      </div>
    ))
  }

  public renderOption (ingredient: IAvailableIngredient, index: number, newIndex: number) {
    return (
      <option
        key={`${ingredient}-${newIndex}-${index}`}
        value={ingredient.id}
      >
        {ingredient.name}
      </option>
    )
  }

  public renderNewIngredients () {
    const { newIngredients } = this.state
    const { availableIngredients } = this.props

    return [...Array(newIngredients)].map((_el, i) => {
      const newIndex = this.getStartingNewIndex() + i
      return (
        <div
          className='flex flex-row items-center'
          key={`${new Date().getMilliseconds()}-${newIndex}`}
        >
          <select
            className='form-field-inline bg-white flex-1'
            name={`recipe[ingredient_recipes_attributes][${newIndex}][ingredient_id]`}
          >
            {availableIngredients.map((ing, index) => (this.renderOption(ing, index, newIndex)))}
          </select>
          <input
            className='form-field-inline flex-1'
            name={`recipe[ingredient_recipes_attributes][${newIndex}][quantity]`}
            type='number'
          />
          <button
            className='btn btn-blue-rev h-full flex-1'
            onClick={this.handleRemoveNewIngredient}
          >
            -
          </button>
        </div>
      )
    })
  }

  public render () {
    const {
      availableIngredients,
      newIngredients,
      ingredients,
      recipe
    } = this.state
    return (
      <div className='form-container'>
        <h1 className='py-2 mb-2'>Edit {recipe.name}</h1>
        <form action={`/recipes/${recipe.id}`} method='post'>
          <input type='hidden' name='_method' value='put' />
          <div>
            <label htmlFor='name'>Name</label>
            <input
              className='form-field'
              name='recipe[name]'
              type='text'
              ref={this.nameField}
              required={true}
            />
          </div>
          <div>
            <label htmlFor='description'>Description</label>
            <input
              className='form-field'
              name='recipe[description]'
              ref={this.descriptionField}
              type='textarea'
            />
          </div>
          <div>
            <span className='my-2'>Ingredients</span>
            {this.renderIngredients()}
            {this.renderNewIngredients()}
          </div>
          <button
            className='btn btn-blue w-full'
            onClick={this.handleClickAddIngredient}
          >
            Add Ingredient
          </button>
          <div className='flex mt-4'>
            <button
              className='btn btn-blue btn-group-left flex-1'
              type='submit'
            >
              Update
            </button>
            <button
              className='btn btn-blue-rev btn-group-right flex-1'
              onClick={this.handleClickBack}
            >
              Back
            </button>
          </div>
          <input
            type='hidden'
            name='authenticity_token'
            value={this.crsfToken}
          />
        </form>
      </div>
    )
  }
}

document.addEventListener('DOMContentLoaded', () => {
  const node = document.getElementById('edit-recipe-form')
  const data = JSON.parse(node!.getAttribute('data') || '')
  ReactDOM.render(<EditRecipeForm {...data} />, node)
})
