export interface IAvailableIngredient {
  id: number | string
  name: string
}

export interface IRecipe {
  id: number
  name: string
  description?: string
}

export interface IIngredient {
  id: number
  name: string
  ingredient_name: string
  ingredient_id: string
  quantity: number
  unit: string
}
