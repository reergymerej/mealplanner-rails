# frozen_string_literal: true

###
# Small helpers for users/auth
##
module SessionsHelper
  def log_in(user)
    binding.pry
    session[:user_id] = user.id
    @current_user = user
  end

  def log_out
    session.delete(:user_id)
    @current_user = nil
  end
end
