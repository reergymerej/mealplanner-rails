# frozen_string_literal: true

##
# Recipes class for server-rendered app
##
class RecipesController < ApplicationController
  before_action :authenticate_user!
  before_action :require_user
  before_action :set_recipe, only: [:edit, :show, :update, :destroy]

  # GET /recipes
  # GET /recipes.json
  def index
    @recipes = Recipe.all.includes(:ingredients)
  end

  # GET /recipes/1
  # GET /recipes/1.json
  def show; end

  def edit
    @available_ingredients = ::Services::Recipes::MapAvailableIngredients
                             .new(recipe: @recipe, user_id: current_user.id)
                             .call
    @ingredients = ::Services::Recipes::MapIngredientsEditData
                   .new(recipe: @recipe).call
  end

  def new
    @recipe = Recipe.new
    @recipe.ingredient_recipes << IngredientRecipe.new
    @available_ingredients = Ingredient.by_user_id(current_user.id)
  end

  # POST /recipes
  # POST /recipes.json
  def create
    @recipe = Recipe.new(recipe_params)
    @recipe.user = current_user
    result = ::Services::Recipes::SetIngredientsService
             .new(recipe: @recipe, params: recipe_params).call

    if result
      redirect_to recipe_path(@recipe)
    else
      flash_errors(@recipe.errors)
      render :new
    end
  end

  # PATCH/PUT /recipes/1
  # PATCH/PUT /recipes/1.json
  def update
    success = ::Services::Recipes::SetIngredientsService
              .new(recipe: @recipe, params: recipe_params).call

    if success
      redirect_to recipes_path
    else
      flash_errors(@recipe.errors) unless @recipe.update(recipe_params)
      render :edit
    end
  end

  # DELETE /recipes/1
  # DELETE /recipes/1.json
  def destroy
    if @recipe.destroy
      redirect_to recipes_path
    else
      render :show
    end
  end

  private

  def set_recipe
    @recipe = Recipe.includes(:ingredients, :ingredient_recipes)
                    .find(params[:id])
  end

  def recipe_params
    params.require(:recipe).permit(:name,
                                   :description,
                                   ingredient_recipes_attributes: [
                                     :ingredient_id,
                                     :quantity
                                   ])
  end
end
