# frozen_string_literal: true

module Api
  module V1
    ##
    # Basic api controller for Ingredients CRUD actions
    ##
    class IngredientsController < ApiController
      before_action :set_ingredient, only: [:show, :update, :destroy]
      before_action :authenticate_user

      # GET /ingredients
      # GET /ingredients.json
      def index
        @ingredients = Ingredient
                       .by_user_id(current_user.id)
                       .includes(:recipes)
      end

      # GET /ingredients/1
      # GET /ingredients/1.json
      def show; end

      # POST /ingredients
      # POST /ingredients.json
      def create
        @ingredient = Ingredient.new(ingredient_params)

        if @ingredient.save
          render :show, status: :created, location: mapped_url(@ingredient)
        else
          render json: mapped_url(@ingredient.errors),
                 status: :unprocessable_entity
        end
      end

      # PATCH/PUT /ingredients/1
      # PATCH/PUT /ingredients/1.json
      def update
        if @ingredient.update(ingredient_params)
          render :show, status: :ok, location: mapped_url(@ingredient)
        else
          render json: mapped_url(@ingredient.errors),
                 status: :unprocessable_entity
        end
      end

      # DELETE /ingredients/1
      # DELETE /ingredients/1.json
      def destroy
        @ingredient.destroy
      end

      private

      attr_reader :current_user

      def set_ingredient
        @ingredient = Ingredient.find(params[:id])
      end

      def ingredient_params
        params.require(:ingredient)
              .permit(:name, :description, :price, :recipes, :unit, :user_id)
      end

      def mapped_url(data)
        api_v1_ingredient_url(data)
      end
    end
  end
end
