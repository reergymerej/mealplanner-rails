# frozen_string_literal: true

module Api
  module V1
    ##
    # Basic api controller for Recipes CRUD actions
    ##
    class RecipesController < ApiController
      before_action :set_recipe, only: [:show, :update, :destroy]
      before_action :authenticate_user

      # GET /recipes
      # GET /recipes.json
      def index
        @recipes = Recipe.all.includes(:ingredients)
      end

      # GET /recipes/1
      # GET /recipes/1.json
      def show; end

      # POST /recipes
      # POST /recipes.json
      def create
        @recipe = Recipe.new(recipe_params)

        if @recipe.save
          render :show, status: :created, location: mapped_url(@recipe)
        else
          binding.pry
          render json: mapped_url(@recipe.errors),
                 status: :unprocessable_entity
        end
      end

      # PATCH/PUT /recipes/1
      # PATCH/PUT /recipes/1.json
      def update
        @recipe.ingredient_ids = params[:ingredients] if params[:ingredients]

        if @recipe.update(recipe_params)
          render :show, status: :ok, location: mapped_url(@recipe)
        else
          render json: mapped_url(@recipe.errors),
                 status: :unprocessable_entity
        end
      end

      # DELETE /recipes/1
      # DELETE /recipes/1.json
      def destroy
        @recipe.destroy
      end

      private

      def set_recipe
        @recipe = Recipe.includes(:ingredients).find(params[:id])
      end

      def recipe_params
        params.require(:recipe)
              .permit(:name, :description, :user_id, ingredients: [],)
      end

      def mapped_url(data)
        api_v1_recipe_url(data)
      end
    end
  end
end
