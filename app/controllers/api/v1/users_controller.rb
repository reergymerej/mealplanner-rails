# frozen_string_literal: true

module Api
  module V1
    ###
    # Base Users Controller
    ##
    class UsersController < ApiController
      before_action :authenticate_user
      before_action :set_user, only: [:show, :update, :destroy]

      def index
        @users = User.all
        render json: @users, status: :ok
      end

      def show
        render json: @user
      end

      def create
        user = User.new(user_params)

        if user.save
          render json: { message: 'Signed up successfully' },
                 status: :created
        else
          render json: { errors: user.errors },
                 status: :bad_request
        end
      end

      def update
        if @user.update(user_params)
          render json: @user
        else
          render json: { errors: @user.errors }, status: bad_request
        end
      end

      def destroy
        @user.destroy
      end

      private

      def set_user
        @user = User.find(params[:id])
      end

      def user_params
        binding.pry
        params.require(:user).permit(:email, :password, :password_confirmation)
      end
    end
  end
end
