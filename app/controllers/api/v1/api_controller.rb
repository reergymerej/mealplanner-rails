# frozen_string_literal: true

##
# Base Application controller class for shared behavior
# of API v1
##
module Api
  module V1
    class ApiController < ActionController::API
      include Knock::Authenticable
      undef_method :current_user
    end
  end
end
