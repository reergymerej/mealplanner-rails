# frozen_string_literal: true

##
# Base Application controller class for shared behavior
##
class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception, prepend: true
  helper_method :logged_in?, :require_user

  private

  def require_user
    return if user_signed_in?

    flash[:alert] = 'You must be logged in'
    redirect_to root_path
  end

  def flash_errors(errors)
    return if errors.nil? || errors.messages.nil?

    formatted = errors.messages.map do |err|
      field_name = err[0].to_s.capitalize
      error_string = err[1]&.first

      "#{field_name} #{error_string}"
    end
    flash[:alert] = formatted.join("\n")
  end
end
