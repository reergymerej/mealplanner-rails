# frozen_string_literal: true

###
# Top-level non-api Users controller
##
class UsersController < ApplicationController
  def show
    @user = User.find(params[:id])
  end

  def new
    @user = User.new
  end

  def create
    user = User.new(user_params)
    if user.save
      log_in user
      flash[:success] = 'Welcome to the Mealplanner'
      redirect_to root_path
    else
      flash_errors(user.errors)
      render 'new'
    end
  end

  private

  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation)
  end

  attr_accessor :user
end
