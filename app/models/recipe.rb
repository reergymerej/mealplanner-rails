# frozen_string_literal: true

##
# Base Recipe model
##
class Recipe < ApplicationRecord
  belongs_to :user

  has_many :ingredient_recipes
  has_many :ingredients, through: :ingredient_recipes

  has_many :mealplan_recipes
  has_many :mealplans, through: :mealplan_recipes

  accepts_nested_attributes_for :ingredient_recipes

  validates :name, presence: true

  def estimated_price
    # TODO: Need to find a good way to use the quantity as a
    # multiplier
    ingredients.reduce(0) { |acc, ing| acc + ing.price }
  end

  scope :by_user_id, ->(id) { where(user_id: id) }
end
