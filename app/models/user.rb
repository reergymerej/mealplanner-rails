# frozen_string_literal: true

###
# Base user class
##
class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :ingredients
  has_many :recipes
  has_many :mealplans

  validates :email, presence: true, uniqueness: { case_sensitive: false }
  validates :password, presence: true, length: { minimum: 8 }

  before_save :downcase_email
  before_save :generate_confirmation_instructions

  alias_method :authenticate, :valid_password?

  def self.from_token_request(request)
    email = request.params['auth'] && request.params['auth']['email']
    self.find_by(email: email)
  end

  def self.from_token_payload(payload)
    self.find(payload['sub'])
  end

  def downcase_email
    self.email = email.delete(' ').downcase
  end

  def generate_confirmation_instructions
    self.confirmation_token = SecureRandom.hex(10)
    self.confirmation_sent_at = Time.now.utc
  end

  def confirmation_token_valid?
    (confirmation_sent_at + 30.days) > Time.now.utc
  end

  def mark_as_confirmed!
    self.confirmation_token = nil
    self.confirmed_at = Time.now.utc
  end
end
