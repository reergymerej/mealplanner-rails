# frozen_string_literal: true

class IngredientRecipe < ApplicationRecord
  belongs_to :ingredient
  belongs_to :recipe

  validates :quantity, presence: true

  scope :by_user_id, ->(id) { where(user_id: id) }

  def ingredient_name
    ingredient&.name
  end

  def ingredient_id
    ingredient&.id
  end

  def ingredient_unit
    ingredient&.unit
  end
end
