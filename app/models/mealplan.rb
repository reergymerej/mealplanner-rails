# frozen_string_literal: true

##
# Top-level class for mealplan, a collection of recipes
##
class Mealplan < ApplicationRecord
  belongs_to :user
  has_many :mealplan_recipes
  has_many :recipes, through: :mealplan_recipes

  scope :by_user_id, ->(id) { where(user_id: id) }
end
