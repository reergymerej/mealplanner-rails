# frozen_string_literal: true

##
# Base Ingredient model
##
class Ingredient < ApplicationRecord
  belongs_to :user
  has_many :ingredient_recipes
  has_many :recipes, through: :ingredient_recipes

  validates :name, presence: true
  validates :price, presence: true

  scope :by_user_id, ->(id) { where(user_id: id) }
end
