module Types
  class MealplanType < Types::BaseObject
    field :id, ID, null: false
    field :recipes, [Types::RecipeType], null: true
    field :user, [Types::UserType], null: false
  end
end
