module Types
  class IngredientType < Types::BaseObject
    field :id, ID, null: false
    field :name, String, null: false
    field :description, String, null: true
    field :price, Float, null: false
    field :unit, String, null: false
    field :quantity, Integer, null: false
    field :recipes, [Types::RecipeType], null: true
    field :user, Types::UserType, null: false
  end
end
