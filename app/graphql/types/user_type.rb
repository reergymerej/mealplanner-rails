module Types
  class UserType < Types::BaseObject
    field :id, ID, null: false
    field :email, String, null: false
    field :ingredients, [Types::IngredientType], null: true
    field :mealplans, [Types::MealplanType], null: true
    field :recipes, [Types::RecipeType], null: true
  end
end
