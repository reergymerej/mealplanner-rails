module Types
  class QueryType < Types::BaseObject
    field :recipes, [Types::RecipeType], null: false do
      description "Returns all recipes"
      argument :user_id, ID, required: true
    end

    def recipes(user_id:)
      Recipe.by_user_id(user_id)
    end
  end
end
