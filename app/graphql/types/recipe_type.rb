module Types
  class RecipeType < Types::BaseObject
    field :id, ID, null: false
    field :name, String, null: false
    field :description, String, null: true
    field :ingredients, [Types::IngredientType], null: true
    field :mealplans, [Types::MealplanType], null: true
    field :user, Types::UserType, null: false
  end
end
