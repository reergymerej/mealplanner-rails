# frozen_string_literal: true

module Services
  module Recipes
    ###
    #   Object to add appropriate ingredients
    ##
    class SetIngredientsService
      def initialize(recipe:, params:)
        @recipe = recipe
        @ingredient_recipes_params = params[:ingredient_recipes_attributes]
                                     .to_h
                                     .with_indifferent_access
      end

      def call
        @recipe.save if @recipe.id.nil?
        ingredient_recipes = build_ingredient_recipes_array()
        return false unless ingredient_recipes.each(&:save)

        @recipe.ingredient_recipes = ingredient_recipes
        @recipe.save
      end

      private

      def build_ingredient_recipes_array
        ingredient_recipes = []
        @ingredient_recipes_params.each_with_index() do |params_hash, i|
          ingredient_recipe = new_ingredient_recipe_from_params(params_hash, i)
          ingredient_recipes.push(ingredient_recipe)
        end
        ingredient_recipes
      end

      def new_ingredient_recipe_from_params(params_hash, index)
        IngredientRecipe.new(
          ingredient_id: extract_ingredient_id(index),
          quantity: extract_quantity(index),
          recipe_id: @recipe.id
        )
      end

      def extract_ingredient_id(index)
        @ingredient_recipes_params[index.to_s][:ingredient_id]&.to_i
      end

      def extract_quantity(index)
        @ingredient_recipes_params[index.to_s][:quantity]&.to_i
      end
    end
  end
end
