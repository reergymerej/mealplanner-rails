# frozen_string_literal: true

module ::Services
  module Recipes
    class MapIngredientsEditData
      def initialize(recipe:)
        @recipe = recipe
      end

      def call
        if @recipe.ingredient_recipes.length <= 0
          return []
        end

        @recipe.ingredient_recipes.reduce([]) do |acc, i|
          acc.push(map_data(i))
          acc
        end
      end

      private

      def map_data(ingredient_recipe)
        return {
          id: ingredient_recipe.id,
          name: ingredient_recipe.ingredient_name,
          quantity: ingredient_recipe.quantity,
          ingredient_id: ingredient_recipe.ingredient_id,
          unit: ingredient_recipe.ingredient_unit
        }
      end
    end
  end
end
