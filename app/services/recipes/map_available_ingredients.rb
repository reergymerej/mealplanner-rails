# frozen_string_literal: true

module ::Services
  module Recipes
    class MapAvailableIngredients
      def initialize(recipe:, user_id:)
        @recipe = recipe
        @user_id = user_id
      end

      def call
        ingredients = Ingredient
                      .by_user_id(@user_id)
                      .includes(:ingredient_recipes)

        ingredients - @recipe.ingredients
      end
    end
  end
end
