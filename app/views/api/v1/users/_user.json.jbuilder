# frozen_string_literal: true

json.extract! user, :id, :email, :created_at, :updated_at, :ingredients
json.url api_v1_user_url(recipe, format: :json)
