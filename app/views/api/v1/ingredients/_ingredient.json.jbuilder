# frozen_string_literal: true

json.extract! ingredient, :id, :name, :description, :price, :unit,
              :created_at, :updated_at, :recipes

json.url api_v1_ingredient_url(ingredient, format: :json)
