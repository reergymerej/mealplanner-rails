# frozen_string_literal: true

json.partial! 'api/v1/recipes/recipe', recipe: @recipe
