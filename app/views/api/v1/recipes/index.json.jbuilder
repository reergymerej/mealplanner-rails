# frozen_string_literal: true

json.array! @recipes, partial: 'api/v1/recipes/recipe', as: :recipe
