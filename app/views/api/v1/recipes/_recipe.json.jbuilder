# frozen_string_literal: true

json.extract! recipe, :id, :name, :description,
              :created_at, :updated_at, :ingredients
json.url api_v1_recipe_url(recipe, format: :json)
