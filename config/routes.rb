# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :users
  root to: 'home#index'
  post 'user_token' => 'user_token#create'

  if Rails.env.development?
    mount GraphiQL::Rails::Engine, at: "/graphiql", graphql_path: "/graphql"
  end

  post "/graphql", to: "graphql#execute"
  resources :recipes
  resources :ingredients
  resources :mealplans

  namespace 'api' do
    namespace 'v1' do
      resources :users
      resources :recipes
      resources :ingredients
    end
  end
end
