class AddUnitToIngredients < ActiveRecord::Migration[5.2]
  def change
    change_table :ingredients do |t|
      t.string :unit
    end
  end
end
