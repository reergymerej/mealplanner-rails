# frozen_string_literal: true

class ChangeIngredientRecipesQuanityDefaultToZero < ActiveRecord::Migration[5.2]
  def change
    change_column_default :ingredient_recipes, :quantity, default: 1
  end
end
