# frozen_string_literal: true

class AddQuantityToIngredientRecipes < ActiveRecord::Migration[5.2]
  def change
    add_column :ingredient_recipes, :quantity, :integer, default: 0
  end
end
