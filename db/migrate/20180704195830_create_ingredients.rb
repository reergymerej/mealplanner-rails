class CreateIngredients < ActiveRecord::Migration[5.2]
  def change
    create_table :ingredients do |t|
      t.string :name, index: true, null: false
      t.text :description
      t.integer :price, index: true, null: false

      t.timestamps
    end
  end
end
