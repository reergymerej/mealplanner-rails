# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mealplan, type: :model do
  describe 'relationships' do
    it { should belong_to(:user) }
    it { should have_many(:recipes) }
  end

  describe 'scopes' do
    describe 'by_user_id' do
      it 'returns only mealplans belonging to current user' do
        user1 = create(:user)
        user2 = create(:user)
        mealplan1 = create(:mealplan, user_id: user1.id)
        mealplan2 = create(:mealplan, user_id: user2.id)

        expect(Mealplan
          .by_user_id(user1.id)
          .include?(mealplan1)).to be(true)

        expect(Mealplan
          .by_user_id(user1.id)
         .include?(mealplan2)).to be(false)

        expect(Mealplan
          .by_user_id(user2.id)
          .include?(mealplan2)).to be(true)

        expect(Mealplan
          .by_user_id(user2.id)
         .include?(mealplan1)).to be(false)
      end
    end
  end
end
