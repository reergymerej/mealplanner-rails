# frozen_string_literal: true

require 'rails_helper'

RSpec.describe IngredientRecipe, type: :model do
  describe 'relationships' do
    it { should belong_to(:ingredient) }
    it { should belong_to(:recipe) }
  end

  describe 'validations' do
    it { should validate_presence_of(:quantity) }
  end
end
