# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MealplanRecipe, type: :model do
  describe 'relationships' do
    it { should belong_to(:mealplan) }
    it { should belong_to(:recipe) }
  end
end
