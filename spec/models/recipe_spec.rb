# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Recipe, type: :model do
  describe 'relationships' do
    it { should have_many(:ingredients) }
    it { should have_many(:mealplans) }
  end

  describe 'validations' do
    it { should validate_presence_of(:name) }
  end

  describe 'scopes' do
    describe 'by_user_id' do
      it 'should return only recipes with current_user id' do
        user1 = create(:user)
        user2 = create(:user)
        recipe1 = create(:recipe, user_id: user1.id)
        recipe2 = create(:recipe, user_id: user2.id)

        actual1 = Recipe.by_user_id(user1.id)
        expect(actual1.include?(recipe1)).to be(true)
        expect(actual1.include?(recipe2)).to be(false)

        actual2 = Recipe.by_user_id(user2.id)
        expect(actual2.include?(recipe1)).to be(false)
        expect(actual2.include?(recipe2)).to be(true)
      end
    end
  end
end
