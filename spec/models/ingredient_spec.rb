# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Ingredient, type: :model do
  describe 'relationships' do
    it { should have_many(:recipes) }
  end

  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:price) }
  end

  describe 'scopes' do
    it 'should have by_user_id' do
      user1 = create(:user)
      user2 = create(:user)
      create(:ingredient, user_id: user1.id)
      create(:ingredient, user_id: user2.id)

      non_scoped = Ingredient.all.to_ary
      scoped_to_user = Ingredient.by_user_id(user1.id).to_ary

      expect(non_scoped.any? { |i| i.user_id == user1.id }).to be(true)
      expect(non_scoped.any? { |i| i.user_id == user2.id }).to be(true)

      expect(scoped_to_user.any? { |i| i.user_id == user1.id }).to be(true)
      expect(scoped_to_user.any? { |i| i.user_id == user2.id }).to be(false)
    end
  end
end
