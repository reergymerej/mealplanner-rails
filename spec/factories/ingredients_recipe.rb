# frozen_string_literal: true

FactoryBot.define do
  factory :ingredient_recipe do
    ingredient_id { create(:ingredient).id }
    recipe_id { create(:recipe).id }
    quantity { Faker::Number.number(1) }
  end
end
