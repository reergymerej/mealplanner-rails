# frozen_string_literal: true

FactoryBot.define do
  factory :ingredient do
    name { Faker::Food.ingredient }
    description { Faker::Food.description }
    price { Faker::Number.number(2) }
    unit { Faker::Lorem::word }
    user
  end
end
