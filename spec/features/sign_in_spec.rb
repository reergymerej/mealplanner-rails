# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'Sign in', type: :feature do
  scenario 'User signs in' do
    user = create(:user)

    visit user_session_path

    expect(page).to have_text('Log in')

    fill_in('Email', with: user.email)
    fill_in('Password', with: user.password)
    click_button('Log in')

    expect(page).to have_text('Sign Out')
  end
end
