# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'Sign in', type: :feature do
  scenario 'User signs up' do
    visit new_user_registration_path

    expect(page).to have_text('Sign up')

    fill_in('Email', with: Faker::Internet::email)

    password = Faker::Books::Lovecraft.fhtagn
    fill_in('Password', with: password)
    fill_in('Password confirmation', with: password)

    click_button('Sign up')

    expect(page).to have_text('Sign Out')
  end
end
