# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Recipes', type: :request do
  let!(:user) { create(:user) }
  let(:user_sign_in_params) {
    { auth: { email: user.email, password: user.password } }
  }

  let!(:jwt) do
    post user_token_path, params: user_sign_in_params
    JSON(response.body)['jwt']
  end

  let(:json_headers) do
    { 'ACCEPT' => 'application/json',
      'Authorization' => "JWT #{jwt}" }
  end

  describe 'GET /recipes' do
    let!(:recipe) do
      recipe = build(:recipe)
      recipe.user_id = user.id
      recipe.save
      recipe
    end

    it 'returns recipes' do
      get api_v1_recipes_path, headers: json_headers
      expect(response).to have_http_status(200)
      json_response = JSON(response.body)
      expect(json_response[0]['name']).to eq(recipe.name)
      expect(json_response[0]['description']).to eq(recipe.description)
    end
  end

  describe 'GET /recipes/:id' do
    let!(:first_recipe) { create(:recipe) }
    let!(:second_recipe) { create(:recipe) }

    it 'returns correct recipe' do
      get "/api/v1/recipes/#{first_recipe.id}", headers: json_headers
      expect(response).to have_http_status(200)

      json_response = JSON(response.body)
      expect(json_response['name']).to eq(first_recipe.name)
      expect(json_response['description']).to eq(first_recipe.description)
    end
  end

  describe 'POST /recipes' do
    it 'returns the successfully created recipe' do
      recipe = {
        name: 'Carrot',
        description: 'Orange pointies',
        user_id: user.id
      }
      recipe_params = { recipe: recipe }
      post '/api/v1/recipes', params: recipe_params, headers: json_headers
      expect(response).to have_http_status(201)

      json_response = JSON(response.body)
      expect(json_response['name']).to eq(recipe[:name])
      expect(json_response['description']).to eq(recipe[:description])
      expect(json_response['price']).to eq(recipe[:price])
      expect(json_response['unit']).to eq(recipe[:unit])
    end
  end

  describe 'PUT /recipes' do
    let!(:recipe) { create(:recipe) }

    it 'returns successfully updated recipe' do
      new_recipe = {
        name: 'Carrot',
        description: 'Orange pointies',
      }

      put "/api/v1/recipes/#{recipe.id}",
          params: { recipe: new_recipe },
          headers: json_headers

      expect(response).to have_http_status(200)

      json_response = JSON(response.body)
      expect(json_response['name']).to eq(new_recipe[:name])
      expect(json_response['description']).to eq(new_recipe[:description])
      expect(json_response['price']).to be(new_recipe[:price])
      expect(json_response['unit']).to eq(new_recipe[:unit])
    end
  end

  describe 'DELETE /recipes/:id' do
    let!(:recipe) { create(:recipe) }

    it 'successfully deletes the recipe' do
      get '/api/v1/recipes', headers: json_headers
      json_response = JSON(response.body)
      expect(json_response.length).to be(1)

      delete "/api/v1/recipes/#{recipe.id}", headers: json_headers

      expect(response).to have_http_status(204)

      get '/api/v1/recipes', headers: json_headers
      json_response = JSON(response.body)
      expect(json_response.length).to be(0)
    end
  end
end
