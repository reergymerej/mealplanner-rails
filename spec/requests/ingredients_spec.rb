# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Ingredients', type: :request do
  let!(:user) { create(:user) }
  let(:user_sign_in_params) {
    { auth: { email: user.email, password: user.password } }
  }
  let!(:jwt) do
    post user_token_path, params: user_sign_in_params
    JSON(response.body)['jwt']
  end

  let(:json_headers) do
    { 'ACCEPT' => 'application/json',
      'Authorization' => "JWT #{jwt}"}
  end

  describe 'GET /ingredients' do
    let!(:ingredient) do
      ingredient = build(:ingredient)
      ingredient.user_id = user.id
      ingredient.save
      ingredient
    end

    it 'returns ingredients' do
      get api_v1_ingredients_path, headers: json_headers
      expect(response).to have_http_status(200)

      json_response = JSON(response.body)
      expect(json_response[0]['name']).to eq(ingredient.name)
      expect(json_response[0]['description']).to eq(ingredient.description)
      expect(json_response[0]['price']).to eq(ingredient.price.to_f.to_s)
      expect(json_response[0]['unit']).to eq(ingredient.unit)
    end
  end

  describe 'GET /ingredients/:id' do
    let!(:first_ingredient) { create(:ingredient) }
    let!(:second_ingredient) { create(:ingredient) }

    it 'returns correct ingredient' do
      get "/api/v1/ingredients/#{first_ingredient.id}", headers: json_headers
      expect(response).to have_http_status(200)

      json_response = JSON(response.body)
      expect(json_response['name']).to eq(first_ingredient.name)
      expect(json_response['description']).to eq(first_ingredient.description)
      expect(json_response['price']).to eq(first_ingredient.price.to_f.to_s)
      expect(json_response['unit']).to eq(first_ingredient.unit)
    end
  end

  describe 'POST /ingredients' do
    it 'returns the successfully created ingredient' do
      ingredient = {
        name: 'Carrot',
        description: 'Orange pointies',
        price: 1,
        unit: 'Stick',
        user_id: user.id
      }
      ingredient_params = { ingredient: ingredient }
      post '/api/v1/ingredients', params: ingredient_params, headers: json_headers

      expect(response).to have_http_status(201)

      json_response = JSON(response.body)
      expect(json_response['name']).to eq(ingredient[:name])
      expect(json_response['description']).to eq(ingredient[:description])
      expect(json_response['price']).to eq(ingredient[:price].to_f.to_s)
      expect(json_response['unit']).to eq(ingredient[:unit])
    end
  end

  describe 'PUT /ingredients' do
    let!(:ingredient) { create(:ingredient) }

    it 'returns successfully updated ingredient' do
      new_ingredient = {
        name: 'Carrot',
        description: 'Orange pointies',
        price: 1,
        unit: 'Stick'
      }

      put "/api/v1/ingredients/#{ingredient.id}",
          params: { ingredient: new_ingredient },
          headers: json_headers

      expect(response).to have_http_status(200)

      json_response = JSON(response.body)
      expect(json_response['name']).to eq(new_ingredient[:name])
      expect(json_response['description']).to eq(new_ingredient[:description])
      expect(json_response['price']).to eq(new_ingredient[:price].to_f.to_s)
      expect(json_response['unit']).to eq(new_ingredient[:unit])
    end
  end

  describe 'DELETE /ingredients/:id' do
    let!(:ingredient) do
      ingredient = build(:ingredient)
      ingredient.user_id = user.id
      ingredient.save
      ingredient
    end

    it 'successfully deletes the ingredient' do
      get '/api/v1/ingredients', headers: json_headers
      json_response = JSON(response.body)
      expect(json_response.length).to be(1)

      delete "/api/v1/ingredients/#{ingredient.id}", headers: json_headers

      expect(response).to have_http_status(204)

      get '/api/v1/ingredients', headers: json_headers
      json_response = JSON(response.body)
      expect(json_response.length).to be(0)
    end
  end
end
